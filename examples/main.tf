module "ionos_cluster" {
  source = "git::https://gitlab.com/ccsolutions.io/open-source/terraform/modules/ionos-k8s-cluster.git?ref=v1.7.3"

  name          = "k8s-cluster-example"
  k8s_version   = "1.30.2"
  datacenter_id = "your-datacenter-id"
  environment   = "your-environment"

  node_pools = {
    default_pool = {
      name        = "default-pool"
      k8s_version = "1.30.2"
      auto_scaling = {
        min_node_count = 1
        max_node_count = 3
      }
      cpu_family        = "INTEL_SKYLAKE"
      availability_zone = "AUTO"
      storage_type      = "SSD"
      storage_size      = 50
      cores_count       = 2
      ram_size          = 4096
      annotations = {
        nodesgroup = "default-pool"
        workflows  = "all"
      }
      lans = {
        id   = "your-lan-id"
        dhcp = true
      }
      maintenance = {
        day_of_the_week = "Sunday"
        time            = "03:00:00Z"
      }
    }
  }
}