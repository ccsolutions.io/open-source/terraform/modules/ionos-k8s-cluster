variable "name" {
  description = "(Required) The name of the Kubernetes Cluster"
  type        = string
}

variable "k8s_version" {
  description = "(Optional) The desired Kubernetes Version"
  type        = string
  default     = "1.30.2"
}

variable "maintenance_window" {
  description = "(Optional) Maintenance Windows for the Cluster"
  type = object({
    day_of_the_week = string
    time            = string
  })
  default = {
    day_of_the_week = "Monday"
    time            = "01:00:00Z"
  }
}

variable "datacenter_id" {
  description = "(Required) A Datacenter's UUID"
  type        = string
}

variable "environment" {
  description = "(Required) Name of the Environment"
  type        = string
}

variable "node_pools" {
  description = "(Required) Spec for the nodes pool"
  type = map(object({
    name = string
    maintenance = object({
      day_of_the_week = string
      time            = string
    })
    k8s_version = string
    auto_scaling = object({
      min_node_count = number
      max_node_count = number
    })
    cpu_family        = string
    availability_zone = string
    storage_type      = string
    storage_size      = number
    cores_count       = number
    ram_size          = number
    annotations = object({
      nodesgroup = string
      workflows  = string
    })
    lans = object({
      id   = number
      dhcp = bool
    })
  }))
  default = {
    k8s_001 = {
      name = "node-pool-001"
      maintenance = {
        day_of_the_week = "Monday"
        time            = "02:30:00Z"
      }
      k8s_version = "1.30.2"
      auto_scaling = {
        min_node_count = 1
        max_node_count = 2
      }
      cpu_family        = "INTEL_SKYLAKE"
      availability_zone = "AUTO"
      storage_type      = "SSD"
      storage_size      = 30
      cores_count       = 2
      ram_size          = 2048
      annotations = {
        nodesgroup = "node-pool-01"
        workflows  = "all-tiers"
      }
      lans = {
        id   = 2
        dhcp = true
      }
    }
  }
}

