resource "ionoscloud_k8s_cluster" "managed_cluster" {
  name        = var.name
  k8s_version = var.k8s_version

  maintenance_window {
    day_of_the_week = lookup(var.maintenance_window, "day_of_the_week", "Monday")
    time            = lookup(var.maintenance_window, "time", "01:00:00Z")
  }

  timeouts {
    create = "30m"
  }
}

resource "ionoscloud_k8s_node_pool" "node_pools" {
  for_each       = var.node_pools
  datacenter_id  = var.datacenter_id
  k8s_cluster_id = ionoscloud_k8s_cluster.managed_cluster.id
  name           = each.value["name"]
  k8s_version    = each.value["k8s_version"]
  maintenance_window {
    day_of_the_week = each.value["maintenance"]["day_of_the_week"]
    time            = each.value["maintenance"]["time"]
  }
  auto_scaling {
    min_node_count = each.value["auto_scaling"]["min_node_count"]
    max_node_count = each.value["auto_scaling"]["max_node_count"]
  }
  cpu_family        = each.value["cpu_family"]
  availability_zone = each.value["availability_zone"]
  storage_type      = each.value["storage_type"]
  node_count        = each.value["auto_scaling"]["min_node_count"]
  cores_count       = each.value["cores_count"]
  ram_size          = each.value["ram_size"]
  storage_size      = each.value["storage_size"]
  labels = {
    owner       = "Terraform"
    environment = var.environment
  }
  annotations = {
    nodesgroup  = each.value["annotations"]["nodesgroup"]
    workflows   = each.value["annotations"]["workflows"]
    environment = var.environment
  }

  lans {
    id   = each.value["lans"]["id"]
    dhcp = each.value["lans"]["dhcp"]
  }

  timeouts {
    create = "30m"
  }

  depends_on = [ionoscloud_k8s_cluster.managed_cluster]
}
