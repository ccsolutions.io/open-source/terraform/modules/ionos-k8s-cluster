data "ionoscloud_k8s_cluster" "k8s_config" {
  id = ionoscloud_k8s_cluster.managed_cluster.id
}

output "id" {
  description = "Ionos Cluster ID"
  value       = ionoscloud_k8s_cluster.managed_cluster.id
}

output "host" {
  description = "Hostname of the Kubernetes API"
  value       = data.ionoscloud_k8s_cluster.k8s_config.config[0].clusters[0].cluster.server
  sensitive   = true
}

output "token" {
  description = "User Token of the Kubernetes API"
  value       = data.ionoscloud_k8s_cluster.k8s_config.config[0].users[0].user.token
  sensitive   = true
}

output "cluster_ca_certificate" {
  description = "The CA Certificate of the Kubernetes API"
  value       = data.ionoscloud_k8s_cluster.k8s_config.ca_crt
  sensitive   = true
}

output "kubeconfig" {
  description = "Kubeconfig file"
  value       = data.ionoscloud_k8s_cluster.k8s_config.kube_config
  sensitive   = true
}