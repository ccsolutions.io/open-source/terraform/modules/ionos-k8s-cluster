# Managed Kubernetes Module for Ionos

---<!-- BEGINNING OF PRE-COMMIT-OPENTOFU DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | 1.7.3 |
| <a name="requirement_ionoscloud"></a> [ionoscloud](#requirement\_ionoscloud) | 6.4.18 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_ionoscloud"></a> [ionoscloud](#provider\_ionoscloud) | 6.4.18 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [ionoscloud_k8s_cluster.managed_cluster](https://registry.terraform.io/providers/ionos-cloud/ionoscloud/6.4.18/docs/resources/k8s_cluster) | resource |
| [ionoscloud_k8s_node_pool.node_pools](https://registry.terraform.io/providers/ionos-cloud/ionoscloud/6.4.18/docs/resources/k8s_node_pool) | resource |
| [ionoscloud_k8s_cluster.k8s_config](https://registry.terraform.io/providers/ionos-cloud/ionoscloud/6.4.18/docs/data-sources/k8s_cluster) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_datacenter_id"></a> [datacenter\_id](#input\_datacenter\_id) | (Required) A Datacenter's UUID | `string` | n/a | yes |
| <a name="input_environment"></a> [environment](#input\_environment) | (Required) Name of the Environment | `string` | n/a | yes |
| <a name="input_k8s_version"></a> [k8s\_version](#input\_k8s\_version) | (Optional) The desired Kubernetes Version | `string` | `"1.30.2"` | no |
| <a name="input_maintenance_window"></a> [maintenance\_window](#input\_maintenance\_window) | (Optional) Maintenance Windows for the Cluster | <pre>object({<br>    day_of_the_week = string<br>    time            = string<br>  })</pre> | <pre>{<br>  "day_of_the_week": "Monday",<br>  "time": "01:00:00Z"<br>}</pre> | no |
| <a name="input_name"></a> [name](#input\_name) | (Required) The name of the Kubernetes Cluster | `string` | n/a | yes |
| <a name="input_node_pools"></a> [node\_pools](#input\_node\_pools) | (Required) Spec for the nodes pool | <pre>map(object({<br>    name = string<br>    maintenance = object({<br>      day_of_the_week = string<br>      time            = string<br>    })<br>    k8s_version = string<br>    auto_scaling = object({<br>      min_node_count = number<br>      max_node_count = number<br>    })<br>    cpu_family        = string<br>    availability_zone = string<br>    storage_type      = string<br>    storage_size      = number<br>    cores_count       = number<br>    ram_size          = number<br>    annotations = object({<br>      nodesgroup = string<br>      workflows  = string<br>    })<br>    lans = object({<br>      id   = number<br>      dhcp = bool<br>    })<br>  }))</pre> | <pre>{<br>  "k8s_001": {<br>    "annotations": {<br>      "nodesgroup": "node-pool-01",<br>      "workflows": "all-tiers"<br>    },<br>    "auto_scaling": {<br>      "max_node_count": 2,<br>      "min_node_count": 1<br>    },<br>    "availability_zone": "AUTO",<br>    "cores_count": 2,<br>    "cpu_family": "INTEL_SKYLAKE",<br>    "k8s_version": "1.30.2",<br>    "lans": {<br>      "dhcp": true,<br>      "id": 2<br>    },<br>    "maintenance": {<br>      "day_of_the_week": "Monday",<br>      "time": "02:30:00Z"<br>    },<br>    "name": "node-pool-001",<br>    "ram_size": 2048,<br>    "storage_size": 30,<br>    "storage_type": "SSD"<br>  }<br>}</pre> | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_cluster_ca_certificate"></a> [cluster\_ca\_certificate](#output\_cluster\_ca\_certificate) | The CA Certificate of the Kubernetes API |
| <a name="output_host"></a> [host](#output\_host) | Hostname of the Kubernetes API |
| <a name="output_id"></a> [id](#output\_id) | Ionos Cluster ID |
| <a name="output_kubeconfig"></a> [kubeconfig](#output\_kubeconfig) | Kubeconfig file |
| <a name="output_token"></a> [token](#output\_token) | User Token of the Kubernetes API |
<!-- END OF PRE-COMMIT-OPENTOFU DOCS HOOK -->